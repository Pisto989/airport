<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Airport extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'code',
        'lat',
        'ing',
        'flight_id'
    ];
    public function flight()
    {
        return $this->belongsToMany('App\Flight');
    }
}
